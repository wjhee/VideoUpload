#  VideoUpload
1：视频上传
2：视频预览
3：图片截取
4: GIF图片截取
5：视频转码


#安装步骤
1：项目根目录下执行composer install
2：配置项目完成后 访问localhost/路由
3：选择视频文件上传，可选取public/source文件下的视频

#常见问题
1：文件大小限值
修改PHP.INI 文件post_max_size ，upload_max_filesize，max_file_uploads大小


#结果
Upload: 2018_03_13_21_17_08_574.mp4
Type: video/mp4
Size: 23480.75390625 Kb
Stored in: C:\Windows\php9117.tmpStored in: upload/2018_03_13_21_17_08_574.mp4
即为操作成功！可发现public下生成了upload文件和frame.jpg图片
