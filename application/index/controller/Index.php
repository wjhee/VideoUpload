<?php
namespace app\index\controller;

use FFMpeg\Coordinate\TimeCode;
use FFMpeg\Coordinate\Dimension;
use FFMpeg\Format\Video\X264;
use FFMpeg\FFMpeg;
use FFMpeg\FFMpegServiceProvider;
use think\App;
use think\console\command\make\Controller;

class Index extends Controller
{
    public function index()
    {
        return view('/upload');
    }

    public function upload()
    {

        if ($_FILES["file"]["error"] > 0) {
            echo "Error: " . $_FILES["file"]["error"] . "<br />";
        } else {
            echo "Upload: " . $_FILES["file"]["name"] . "<br />";
            echo "Type: " . $_FILES["file"]["type"] . "<br />";
            echo "Size: " . ($_FILES["file"]["size"] / 1024) . " Kb<br />";
            echo "Stored in: " . $_FILES["file"]["tmp_name"];
            $dirPath = 'upload/';//设置文件保存的目录
            if (!is_dir($dirPath)) {
                //目录不存在则创建目录
                @mkdir($dirPath);

            }
            if (file_exists("upload/" . $_FILES["file"]["name"])) {
                echo $_FILES["file"]["name"] . " already exists. ";
            } else {
                move_uploaded_file($_FILES["file"]["tmp_name"],
                    "upload/" . $_FILES["file"]["name"]);
                echo "Stored in: " . "upload/" . $_FILES["file"]["name"];
            }
            $ffmpeg = FFMpeg::create(array(
                'ffmpeg.binaries' => 'D:\phpstudy\PHPTutorial\php\php-7.2.1-nts\ffmpeg\bin\ffmpeg.exe',
                'ffprobe.binaries' => 'D:\phpstudy\PHPTutorial\php\php-7.2.1-nts\ffmpeg\bin\ffprobe.exe',
                'timeout' => 0,
                'ffmpeg.threads' => 12
            ));
            $video = $ffmpeg->open("upload/" . $_FILES["file"]["name"]);
            //截图图片
            $video->frame(TimeCode::fromSeconds(20))->save('upload/frame.jpg');
            //截取gif
            $video->gif(TimeCode::fromSeconds(20),new Dimension(300,400),100)->save('upload/frame1.gif');
        }

    }


}
